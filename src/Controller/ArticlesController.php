<?php
namespace App\Controller;
class ArticlesController extends AppController
{
    var $helper = array('Html','Session');
    //var $components = array('Data','Session');
	//phân trang
	//public $helpers = array('Paginator','Html');
	public $paginate = [
		'fields' => [],//hiển thị
        'limit' => 3,
        'order' => [ 'Articles.title' => 'asc'],
        //'finder' => 'published'
    ];
	public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Flash'); // Include các thành phần flash
        $this->loadComponent('Paginator');//thành phần phân trang
    }

	// tìm các bài viết bởi tag
    public function tags()
    {
        $tags = $this->request->getParam('pass');
        $customFinderOptions = [
            'tags' => $tags
        ];
        // phương pháp finder custom được gọi là findTagged bên trong ArticlesTable.php
        // nó sẽ giống như sau:
        // public function findTagged(Query $query, array $options) {
        // nên bạn sử dụng tagged as the key
        $this->paginate = [
            'finder' => [
                'tagged' => $customFinderOptions
            ]
        ];
        $articles = $this->paginate($this->Articles);
        $this->set(compact('articles', 'tags'));
    }

	public function index()
    {   
        //$this->set('articles', $this->Articles->find('all'));
        $this->set('articles', $this->paginate());//thực hiện phân trang trong index
    }

    public function view($id = null)
    {
        $article = $this->Articles->get($id);
        $this->set(compact('article'));
    }

    public function add()
    {
        $article = $this->Articles->newEntity();
        if ($this->request->is('post')) {
            $article = $this->Articles->patchEntity($article, $this->request->getData());
            // Thêm dòng:
        	$article->user_id = $this->Auth->user('id');
        	// Cũng có thể thực hiện như sau: 
        	// $newData = ['user_id' => $his->Auth->user('id')]; 
        	// $article = $this->Articles->patchEntity($article, $newData);
            if ($this->Articles->save($article)) {
                $this->Flash->success(__('Your article has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to add your article.'));
        }
        $this->set('article', $article);

        // Chỉ cần thêm danh sách các loại để có thể chọn 
	    // một thể loại cho một bài viết 
	    /*$categories = $this->Articles->Categories->find('treeList');
    	$this->set(compact('categories'));*/
    }

    public function edit($id = null)
	{
	    $article = $this->Articles->get($id);
	    if ($this->request->is(['post', 'put'])) {
	        $this->Articles->patchEntity($article, $this->request->getData());
	        if ($this->Articles->save($article)) {
	            $this->Flash->success(__('Your article has been updated.'));
	            return $this->redirect(['action' => 'index']);
	        }
	        $this->Flash->error(__('Unable to update your article.'));
	    }

	    $this->set('article', $article);
	}

	public function delete($id)
	{
	    $this->request->allowMethod(['post', 'delete']);

	    $article = $this->Articles->get($id);
	    if ($this->Articles->delete($article)) {
	        $this->Flash->success(__('The article with id: {0} has been deleted.', h($id)));
	        return $this->redirect(['action' => 'index']);
	    }
	}

	public function isAuthorized($user)
	{
	    // Tất cả người dùng đã đăng ký có thể thêm các bài viết
	    if ($this->request->getParam('action') === 'add') {
	        return true;
	    }

	    // Chủ sở hữu của một bài báo có thể chỉnh sửa và xóa nó
	    if (in_array($this->request->getParam('action'), ['edit', 'delete'])) {
	        $articleId = (int)$this->request->getParam('pass.0');
	        if ($this->Articles->isOwnedBy($articleId, $user['id'])) {
	            return true;
	        }
	    }
	    return parent::isAuthorized($user);
	}

}
?>