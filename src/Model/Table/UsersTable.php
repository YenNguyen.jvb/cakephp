<?php
namespace App\Model\Table;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class UsersTable extends Table
{
    public function validationDefault(Validator $validator)
    {
        return $validator
            ->notEmpty('username')//yêu cầu nhập đủ thông tin
            ->notEmpty('password')
            ->notEmpty('role')
            ->add('role', 'inList', ['rule' => ['inList', ['admin', 'author']],'message' => 'Vui lòng nhập giá trị hợp lệ']);
    }
}
?>