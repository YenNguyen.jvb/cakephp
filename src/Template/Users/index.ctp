﻿<h1>Blog users</h1>
<p><?= $this->Html->link('Add User', ['action' => 'add']) ?></p>
<table>
    <tr>
        <th>Id</th>
        <th>Username</th>
        <th>Role</th>
        <th>Action</th>
    </tr>

    <!-- Here is where we iterate through our $users query object, printing out article info -->

    <?php foreach ($users as $user): ?> <!--lặp đối tượng truy vấn, xuất ra thông tin-->
    <tr>
        <td><?= $user->id ?></td>
        <td>
            <?= $this->Html->link($user->username, ['action' => 'view', $user->id]) ?>
        </td>
        <td>
            <?= $user->role ?>
        </td>
        <td>
            <?= $this->Form->postLink('Delete',['action' => 'delete', $user->id],['confirm' => 'Bạn chắc chắn muốn xóa?']) ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>